from app.conf.environ import env

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "app.middleware.real_ip.real_ip_middleware",
]

if not env("DEBUG", cast=bool, default=False):
    MIDDLEWARE.insert(2, "whitenoise.middleware.WhiteNoiseMiddleware")
else:
    MIDDLEWARE.insert(len(MIDDLEWARE) - 2, "debug_toolbar.middleware.DebugToolbarMiddleware")
