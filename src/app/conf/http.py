from app.conf.environ import env

ALLOWED_HOSTS = ["*"]  # host validation is not necessary in 2020

CSRF_TRUSTED_ORIGINS = [
    "your.app.origin",
]

CORS_ORIGIN_REGEX_WHITELIST = [
    r".*localhost.*",
    r".*127.0.0.1.*",
    r".*192.168.*",
    # add other origins
]

CORS_ALLOW_CREDENTIALS = env("CORS_ALLOW_CREDENTIALS", cast=bool, default=False)
