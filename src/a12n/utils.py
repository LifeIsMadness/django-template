from rest_framework_simplejwt.tokens import RefreshToken

from users.models import User


def get_jwt(user: User) -> dict:
    """Make JWT for given user"""
    refresh = RefreshToken.for_user(user)

    return {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }
