import pytest

from freezegun import freeze_time

from a12n.utils import get_jwt

pytestmark = [
    pytest.mark.django_db,
    pytest.mark.freeze_time("2049-01-05"),
]


@pytest.fixture
def refresh_token(as_user):
    def _refresh_token(token, expected_status=200):
        return as_user.post(
            "/api/v1/auth/token/refresh/",
            {
                "refresh": token,
            },
            format="json",
            expected_status=expected_status,
        )

    return _refresh_token


@pytest.fixture
def initial_tokens(as_user):
    with freeze_time("2049-01-03"):
        return get_jwt(as_user.user)


def test_refresh_token_ok(initial_tokens, refresh_token):
    result = refresh_token(initial_tokens["refresh"])

    assert "access" in result


def test_rotation_token_ok(initial_tokens, refresh_token):
    result = refresh_token(initial_tokens["refresh"])

    assert "refresh" in result


def test_refreshed_token_is_a_token(initial_tokens, refresh_token):
    result = refresh_token(initial_tokens["refresh"])

    assert len(result["access"]) > 32


def test_refreshed_token_is_new_one(initial_tokens, refresh_token):
    result = refresh_token(initial_tokens["refresh"])

    assert result["access"] != initial_tokens["access"]


def test_refresh_token_after_rotation_is_new(initial_tokens, refresh_token):
    result = refresh_token(initial_tokens["refresh"])

    assert result["refresh"] != initial_tokens["refresh"]


def test_refresh_token_fails_with_incorrect_previous_token(refresh_token):
    result = refresh_token("some-invalid-previous-token", expected_status=401)

    assert "detail" in result


def test_token_is_not_allowed_to_refresh_if_expired(initial_tokens, refresh_token):
    # ensure to use correct date(depends on the lifetime of the refresh token)
    with freeze_time("2049-03-05"):
        result = refresh_token(initial_tokens["refresh"], expected_status=401)  # noqa

    assert "expired" in result["detail"]


def test_received_token_works(as_anon, refresh_token, initial_tokens):
    token = refresh_token(initial_tokens["refresh"])["access"]
    as_anon.credentials(HTTP_AUTHORIZATION=f"Bearer {token}")

    result = as_anon.get("/api/v1/users/me/")

    assert "username" in result
