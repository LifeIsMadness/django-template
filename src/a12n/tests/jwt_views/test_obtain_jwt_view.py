import json
import pytest

pytestmark = pytest.mark.django_db


@pytest.fixture
def get_token(as_user):
    def _get_token(email, password, expected_status=200):
        return as_user.post(
            "/api/v1/auth/token/",
            {
                "email": email,
                "password": password,
            },
            format="json",
            expected_status=expected_status,
        )

    return _get_token


def _decode(response):
    return json.loads(response.content.decode("utf-8", errors="ignore"))


def test_getting_token_ok(as_user, get_token):
    result = get_token(as_user.user.email, as_user.password)

    assert "refresh" in result
    assert "access" in result


def test_getting_token_is_token(as_user, get_token):
    result = get_token(as_user.user.email, as_user.password)

    assert len(result["refresh"]) > 32  # every stuff that is long enough, may be a JWT token
    assert len(result["access"]) > 32


def test_getting_token_with_incorrect_password(as_user, get_token):
    result = get_token(as_user.user.email, "z3r0c00l", expected_status=401)

    assert "detail" in result


@pytest.mark.parametrize(
    ("extract_token", "status_code", "expected_result"),
    [
        (lambda response: response["access"], 200, "email"),
        (
            lambda *args: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRpbW90aHk5NSIsImlhdCI6MjQ5MzI0NDgwMCwiZXhwIjoyNDkzMjQ1MTAwLCJqdGkiOiI2MWQ2MTE3YS1iZWNlLTQ5YWEtYWViYi1mOGI4MzBhZDBlNzgiLCJ1c2VyX2lkIjoxLCJvcmlnX2lhdCI6MjQ5MzI0NDgwMH0.YQnk0vSshNQRTAuq1ilddc9g3CZ0s9B0PQEIk5pWa9I",
            401,
            "detail",
        ),
        (lambda *args: "sh1t", 401, "detail"),
    ],
)
def test_received_token_works(as_user, get_token, as_anon, extract_token, status_code, expected_result):
    token = extract_token(get_token(as_user.user.email, as_user.password))
    as_anon.credentials(HTTP_AUTHORIZATION=f"Bearer {token}")

    result = as_anon.get("/api/v1/users/me/", expected_status=status_code)

    assert expected_result in result
