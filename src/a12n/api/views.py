from drf_spectacular.utils import extend_schema
from drf_spectacular.utils import inline_serializer
from rest_framework import status
from rest_framework_simplejwt import views as jwt

from a12n.api.throttling import AuthAnonRateThrottle


class ObtainJSONWebTokenView(jwt.TokenObtainPairView):
    throttle_classes = [AuthAnonRateThrottle]


class RefreshJSONWebTokenView(jwt.TokenRefreshView):
    throttle_classes = [AuthAnonRateThrottle]

    @extend_schema(
        responses={status.HTTP_200_OK: inline_serializer("TokenRefreshSerializer", {"refresh": str, "access": str})},
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
